import { createStore, combineReducers } from "redux"
import api_images_reducer from "./reducers/api_images_reducer"


const root_reducer = combineReducers({
     api_images_reducer,
})

const store = createStore(root_reducer)

export default store;