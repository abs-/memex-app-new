import React, { Component } from 'react';
import './App.css';
import { connect } from "react-redux"
import ImageSelection from './components/ImageSelection/ImageSelection';
import { Row, Col } from 'antd';

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div className="App">
        <Row>

          <Col span={24}>
            <div style={{ fontSize: 40, margin: 15, color: "white" }}>
              Meme generator
            </div>

            <div style={{ fontSize: 20, margin: 15, color: "white" }}>
              Create a meme from JPEG or PNG images.
            </div>

            <div style={{ fontSize: 15, margin: 10, color: "white" }}>
              Edit your image and make a meme.
            </div>

          </Col>

          <Col span={24} style={{ marginTop: 40 }}>
            <ImageSelection history={this.props.history} />
          </Col>
        </Row>

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    canvas_saved_images: state.api_images_reducer
  }
}

export default connect(mapStateToProps)(App);
