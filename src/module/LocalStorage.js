const save_image_local_storage = ({ canvas }) => {
    let canvas_images = JSON.parse(localStorage.getItem("canvas_images"));
    if (canvas_images == undefined) canvas_images = []
    canvas_images[canvas_images.length] = (canvas.toString())
    localStorage.setItem("canvas_images", JSON.stringify(canvas_images));
}



const get_saved_local_images = ({ key }) => {
    return new Promise((resolved, reject) => {
        let canvas_images = JSON.parse(localStorage.getItem("canvas_images"));
        resolved(canvas_images)
    })
}

export const LocalStorage = {
    save_image_local_storage,
    get_saved_local_images
}