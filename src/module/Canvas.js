var by_pass_cores = "https://cors-anywhere.herokuapp.com/"

const file_image_upload = ({ file, ref }) => {
    return new Promise((resolved, reject) => {
        const node = ref.current;
        var ctx = node.getContext('2d');
        var reader = new FileReader();
        reader.onload = function (event) {
            var img = new Image();
            img.onload = function () {
                node.width = img.width;
                node.height = img.height;
                ctx.drawImage(img, 0, 0);
            }
            img.onerror = function (error) {
                reject(error.type)
            }
            img.src = event.target.result;
            resolved({ src: img.src, canvas: node })
        }
        reader.readAsDataURL(file);
    })
}

const update_canvas = ({ canvas_, text, img, position, color }) => {
    var canvas = canvas_.current;
    var current_image = img.current
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.drawImage(current_image, 0, 0);
    ctx.font = "30px Arial";

    if (color) {
        ctx.fillStyle = color.toString();
    }

    if (position == "top") {
        ctx.textBaseline = "top"
        ctx.fillText(text, canvas.width / 2 - text.length * 5, 0);
    }

    if (position == "middle") {
        ctx.textBaseline = "middle"
        ctx.fillText(text, canvas.width / 2 - text.length * 5, canvas.height / 2);
    }

    if (position == "bottom") {
        ctx.textBaseline = "bottom"
        ctx.fillText(text, canvas.width / 2 - text.length * 5, canvas.height);
    }

}

const save_canvas_image = ({ canvas, img, text }, callback) => {
    try {
        var jpeg = canvas.current.toDataURL("image/jpeg", 0.7);
        callback && callback(null, jpeg)
    } catch (e) {
        callback && callback("error")
    }
}


const remote_image = ({ src, canvas }) => {
    return new Promise((resolved, reject) => {
        var img = new Image();
        var canvas_current_node = canvas.current;
        var ctx = canvas_current_node.getContext('2d');
        img.onload = function () {
            canvas_current_node.width = img.width;
            canvas_current_node.height = img.height;
            ctx.drawImage(img, 0, 0);
            resolved({ src: canvas_current_node.toDataURL("image/jpeg", 0.7), canvas })
        }
        img.onerror = function (error) {
            reject(error.type)
        }
        img.setAttribute('crossOrigin', 'anonymous');
        img.src = by_pass_cores + src;
    })
}


const clear_canvas = ({ canvas }) => {
    return new Promise((resolved, reject) => {
        var canvas_current_node = canvas.current;
        var ctx = canvas_current_node.getContext('2d');
        ctx.clearRect(0, 0, 0, 0);
        resolved("done")
    })
}



export const Canvas = {
    file_image_upload,
    update_canvas,
    save_canvas_image,
    remote_image,
    clear_canvas
}