import React, { Component } from 'react';
import { connect } from "react-redux"
import { Row, Col, Empty } from 'antd';
import './Saved.css';

class Saved extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            saved_images: undefined
        }
    }

    componentDidMount() {
        // LocalStorage.get_saved_local_images({ key: "canvas_images" }).then((response) => {}).catch((e) => { })
    }

    return_image_cards() {
        return <div className={"mobile_view"} style={{ flexWrap: "wrap", display: "flex" }}>
            {
                this.props.canvas_saved_images.canvas_saved_images.map((value, index) => {
                    return <div style={{ margin: 10 }}>
                        <a onClick={() => {
                            var url = value.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
                            window.open(url);
                        }} >
                            <img style={{ width: 200 }} src={value.toString()} alt="" />
                        </a>
                    </div>
                })
            }
        </div >

    }

    render() {
        let { canvas_saved_images } = this.props;

        return (
            <div className="App">
                <Row>
                    <Col span={24}><div style={{ fontSize: 40, margin: 15, color: "white" }}>Meme generator</div><div style={{ fontSize: 20, margin: 15, color: "white" }}>Create a meme from JPEG or PNG images.</div><div style={{ fontSize: 15, margin: 10, color: "white" }}>Edit your image and make a meme.</div>
                    </Col>

                    <Col span={24} style={{ marginTop: 40, display: "flex", justifyContent: "space-evenly" }}>
                        <div style={{ width: "80%", textAlign: "center" }}>
                            {canvas_saved_images && canvas_saved_images.canvas_saved_images && canvas_saved_images.canvas_saved_images.length > 0 ? this.return_image_cards() : <Empty style={{ color: "white" }} />}
                        </div>
                    </Col>
                </Row>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        canvas_saved_images: state.api_images_reducer
    }
}

export default connect(mapStateToProps)(Saved);
