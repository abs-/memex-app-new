import React, { Component } from 'react';
import './ImageSelection.css';
import { connect } from "react-redux"
import { Dropdown, Button, } from 'antd';
import { DownOutlined } from '@ant-design/icons';

import { Canvas } from "../../module/Canvas"
import { notification, Select, Input, Row, Col, Popover } from 'antd';
import ApiModel from '../ApiModel/ApiModel';
import { Api } from '../../module/Api';
import { get_api_images, save_canvas_images } from '../../actions/api_images';
import { } from 'antd';




const { Search } = Input;
const { Option } = Select;


class ImageSelection extends React.Component {
    canvas_ref;
    hidden_image_ref;


    constructor(props) {
        super(props)
        this.state = {
            url_image: null,
            file_image: null,
            api_url: null,
            is_visible_drop: false,
            show_api_model: false,
            canvas: undefined,
            update_canvas_text: undefined,
            update_text_position: undefined,
            update_color: undefined
        }

        this.canvas_ref = React.createRef();
        this.hidden_image_ref = React.createRef();


        this.uploadFile = this.uploadFile.bind(this)
        this.save_image = this.save_image.bind(this)


    }

    update_canvas_position = (canvas) => {
        const image_node = this.hidden_image_ref.current;
        image_node.src = canvas.src

        setTimeout(() => {
            let { current } = this.canvas_ref
            this.setState({ canvas: { height: current.height, width: current.width } })
        }, 100)

    }


    uploadFile = (event) => {
        let file = event.target.files[0];
        Canvas.file_image_upload({ file: file, ref: this.canvas_ref }).then(canvas => {
            if (canvas) {
                this.update_canvas_position(canvas)
            }
        }).catch((e) => {
            this.openNotificationWithIcon("error", "Invalid Image", "")
        })
    }

    reset_canvas = () => {

    }

    remote_image_upload = () => {
        var regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (!regexp.test(this.state.url_image)) {
            this.openNotificationWithIcon("error", "Invalid url")
            this.setState({ url_image: null })
            return true;
        }

        Canvas.remote_image({ src: this.state.url_image, canvas: this.canvas_ref }).then((canvas) => {
            if (canvas) {
                this.setState({ is_visible_drop: false })
                this.update_canvas_position(canvas)
            }
        }).catch((e) => {
            this.openNotificationWithIcon("error", "Invalid url", "Invalid")
            this.setState({ url_image: null })
        })
    }

    openNotificationWithIcon = (type, title, txt) => {
        notification[type]({
            message: title,
            description: txt,
        });
    };

    image_to_canvas = ({ url }) => {
        Canvas.remote_image({ src: url, canvas: this.canvas_ref }).then((canvas) => {
            if (canvas) {
                this.update_canvas_position(canvas)
            }
        }).catch((e) => {
            this.openNotificationWithIcon("error")
        })
    }


    return_overlap_menu = () => {
        return (
            <div style={{ backgroundColor: "#30343F" }}>

                <div style={{ padding: 3, paddingLeft: 0, paddingRight: 0 }}>
                    <Input accept="image/x-png,image/gif,image/jpeg" onChange={this.uploadFile} type={"file"} style={{ width: 250, height: 38 }} placeholder="input Input text" />
                </div>

                <div style={{ padding: 3, paddingLeft: 0, paddingRight: 0 }}>
                    <Input placeholder={"paste image path"} value={this.state.url_image} onChange={(event) => { if (event.nativeEvent.inputType === "insertFromPaste") { this.setState({ url_image: event.nativeEvent.target.value }, () => this.remote_image_upload()) } }} style={{ width: 250, height: 38 }} />
                </div>

                <div style={{ padding: 3, paddingLeft: 0, paddingRight: 0 }}>
                    <Input placeholder={"paste api path"} value={this.state.api_url} onChange={(event) => { if (event.nativeEvent.inputType === "insertFromPaste") { this.setState({ api_url: event.nativeEvent.target.value }, () => this.api_call()) } }} style={{ width: 250, height: 38 }} />
                </div>

            </div>
        )
    }

    api_call = () => {
        this.setState({ show_api_model: true, is_visible_drop: false }, () => {
            Api.get({ url: this.state.api_url }).then(response => {
                get_api_images({ dispatch: this.props.dispatch, images: response })
            }).catch(e => {
                this.setState({ show_api_model: false })
                this.openNotificationWithIcon("error", "Invalid", "Api Invalid Endpoint")
            })
        })
    }

    hide_api_model = flag => this.setState({ show_api_model: flag })


    save_image = () => {
        if (!this.state.update_canvas_text) {
            this.openNotificationWithIcon("error", "Please enter the caption")
            return;
        }

        if (!this.state.update_text_position) {
            this.openNotificationWithIcon("error", "Please select the position")
            return;
        }

        Canvas.save_canvas_image({ canvas: this.canvas_ref }, ((error, response) => {
            if (!error) {
                this.openNotificationWithIcon("success", "Saved", "Image saved")
                save_canvas_images({ dispatch: this.props.dispatch, data: response });
                this.props.history.push("/saved")
            }

        }))
    }


    return_text_ui = () => {
        return (
            <div>

                <div style={{ padding: 2 }}>
                    <Input value={this.state.update_canvas_text} onChange={(event) => {
                        this.setState({ update_canvas_text: event.target.value })
                        Canvas.update_canvas({ canvas_: this.canvas_ref, text: event.target.value, img: this.hidden_image_ref, position: this.state.update_text_position, color: this.state.update_color });
                    }} placeholder="Caption" />
                </div>


                <div style={{ padding: 2 }}>
                    <Select
                        style={{ width: 200, fontSize: 13 }}
                        placeholder="Select Position Vertical"
                        optionFilterProp="children"
                        onChange={(e) => {
                            this.setState({ update_text_position: e })
                            Canvas.update_canvas({ canvas_: this.canvas_ref, text: this.state.update_canvas_text || "", img: this.hidden_image_ref, position: e, color: this.state.update_color });
                        }}>
                        <Option value="top">Top</Option>
                        <Option value="middle">Center</Option>
                        <Option value="bottom">Bottom</Option>
                    </Select>
                </div>

                <div style={{ padding: 2 }}>
                    <Input type="color" value={this.state.update_color} onChange={(event) => {
                        this.setState({ update_color: event.target.value })
                        Canvas.update_canvas({
                            canvas_: this.canvas_ref,
                            text: this.state.update_canvas_text,
                            img: this.hidden_image_ref,
                            position: this.state.update_text_position,
                            color: event.target.value
                        });
                    }} placeholder="Color" />
                </div>
                <div style={{ padding: 2, textAlign: "right", marginTop: 5 }}>
                    <Button type="primary" ghost onClick={() => {
                        this.save_image()
                    }}>Save Meme</Button>
                </div>
            </div>
        )
    }


    render() {
        return (
            <div className="App">
                <Row>
                    <Col span={24}>
                        <Dropdown overlay={this.return_overlap_menu()} visible={this.state.is_visible_drop} onVisibleChange={(flag) => {
                            flag ? this.setState({ is_visible_drop: true }) : this.setState({ is_visible_drop: false, file_image: null, api_url: null, url_image: null })
                        }}>
                            <Button className={"btnOptions"}>
                                Select Image <DownOutlined style={{ fontSize: 25 }} />
                            </Button>
                        </Dropdown>
                    </Col>

                    <Col span={24} style={{ justifyContent: "center", display: "flex" }}>
                        {
                            this.state.canvas && this.state.canvas.width ?
                                <div style={{ width: this.state.canvas.width, padding: 10, textAlign: "left", marginTop: 20, }}>
                                    <Popover placement="topLeft" title={""} content={this.return_text_ui()} trigger="click">
                                        <a style={{ fontSize: 20, color: "white", textDecoration: "underline", fontWeight: "bolder" }}>Text</a>
                                    </Popover>
                                </div>
                                : null
                        }
                    </Col>

                    <Col span={24}>
                        <div style={{ marginTop: 20, justifyContent: "center", display: "flex" }}>
                            <canvas ref={this.canvas_ref} id="canvas" style={{}} />
                            <img id="canvas_image" style={{ display: "none" }} ref={this.hidden_image_ref} alt="" />
                        </div>
                    </Col>

                    <Col span={24}>
                        {this.state.show_api_model ? <ApiModel hide_api_model={this.hide_api_model} image_to_canvas={this.image_to_canvas} /> : null}
                    </Col>

                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        _home_reducer: state._home_reducer
    }
}

export default connect(mapStateToProps)(ImageSelection);
