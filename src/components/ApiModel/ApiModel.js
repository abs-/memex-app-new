import React, { Component } from 'react';
import './ApiModel.css';
import { connect } from "react-redux"
import { Row, Col } from 'antd';
import { Modal, Button, Empty } from 'antd';
import { Spin, Alert } from 'antd';

import { Card } from 'antd';

const { Meta } = Card;


class ApiModel extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            visible: true
        }
    }

    render_images_block = () => {
        let { images } = this.props.api_images_reducer;
        if (images == undefined) {
            return <div style={{textAlign:"center"}}><Spin size="large" /></div>
        }

        if (images && images.data && images.data.images && images.data.images.length == 0) {
            return <div style={{ justifyContent: "center", display: "flex" }}><Empty /></div>
        }


        if (images && images.data && images.data.images && images.data.images.length > 0) {
            return (
                <div className="image_container">
                    {
                        images.data.images.map((value, index) => {
                            return <Card
                                onClick={() => {
                                    this.props.image_to_canvas({ url: value.url })
                                    this.props.hide_api_model(false)
                                }}
                                hoverable
                                style={{ width: "45%", margin: 5 }}
                                cover={<img alt="example" src={value.url} style={{}} />} >
                            </Card>

                        })
                    }
                </div>
            )
        }
    }

    render() {
        return (
            <div>
                <Modal
                    size="small"
                    title="Images"
                    style={{ height: 400 }}
                    visible={this.state.visible}
                    onOk={() => { this.props.hide_api_model(false) }}
                    onCancel={() => { this.props.hide_api_model(false) }}
                >
                    {
                        this.render_images_block()
                    }
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        api_images_reducer: state.api_images_reducer
    }
}

export default connect(mapStateToProps)(ApiModel);
