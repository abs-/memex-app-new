import { GET_API_IMAGES, SAVE_CANVAS_IMAGES } from "../actions/api_images";
import { LocalStorage } from "../module/LocalStorage";

const initState = {
    images: undefined,
    canvas_saved_images: []
}

const api_images_reducer = (state = initState, action) => {

    switch (action.type) {
        case GET_API_IMAGES:
            return {
                ...state,
                images: action.payload
            }
            break;


        case SAVE_CANVAS_IMAGES:
            let col = state.canvas_saved_images;
            col.push(action.payload)
            LocalStorage.save_image_local_storage({ canvas: col })
            return {
                ...state,
                canvas_saved_images: col
            }
            break;

        default:
            return state;

    }
}


export default api_images_reducer;
