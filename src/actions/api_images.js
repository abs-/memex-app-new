

export const GET_API_IMAGES = "GET_API_IMAGES"

export const get_api_images = ({ dispatch, images }) => {
    dispatch({
        type:GET_API_IMAGES,
        payload:images
    })
}




export const SAVE_CANVAS_IMAGES = "SAVE_CANVAS_IMAGES"

export const save_canvas_images = ({ dispatch, data }) => {
    dispatch({
        type:SAVE_CANVAS_IMAGES,
        payload:data
    })
}





